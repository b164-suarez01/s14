console.log("Hello World");

let person = {
	fName: "John",
	lName: "Smith",
	age:30,
	hobbies: ["Biking","Mountain Climbing","Swimming"],
	workAddress: {
		houseNumber: 32,
		street: "Washington",
		city:"Lincoln",
		state:"Nebraska",
	},
	isMarried: true,
	printUserInfo: function(fName,lName,age){
	console.log(`${fName} ${lName} is ${age} years of age.`);
	console.log(`This was printed inside of the function.`);
	console.log(this.hobbies);
	console.log(`This was printed inside of the function.`);
	console.log(this.workAddress);
	},
};

console.log(person);
person.printUserInfo(person.fName,person.lName,person.age);


let returnFunction = function(){
	return person.isMarried;
}
console.log(`The value of isMarried is ${returnFunction()}.`);


//STRETCH GOALS
//1
let input1 = prompt("What is your first number?");
let input2 = prompt("What is your second number?");


function addition(num1,num2) {
	return Number(num1) + Number(num2);
};

let total = addition(input1,input2);
console.log(`The result of the addition is ${total}`); 

//2
function subtraction(num1,num2) {
	return num1-num2;
};

let difference = subtraction(input1,input2);
console.log(`The result of the subtraction is ${difference}`); 


//3
function multiplication(num1,num2) {
	return num1*num2;
};

let product = multiplication(input1,input2);
console.log(`The result of the multiplication is ${product}`); 


