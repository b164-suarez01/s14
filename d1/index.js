console.log("Hello World");

let myVariable;
console.log(myVariable);

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

productName	= "Laptop";
console.log(productName);

const pi = 3.14;

let supplier = "John";
console.log(supplier);

supplier = "Zuitt";
console.log(supplier);

let outerVariable = "hello";

{let innerVariable = "Hello again";
console.log(innerVariable);

}

console.log(outerVariable);

let productCode = "DCO17";
let productBrand = "Dell";

console.log(productCode,productBrand);

//Data Types
//STRINGS - are a series of characters that create a word, a phrase, a sentecnce or anything related to creating a text.
//Stringd in JS can be written using either a single ('') or double "" quotation.


let country = "Philippine's";
let trial = "Henry's";

console.log(country);
console.log(trial);

let fullAddress = country + " " + trial;
console.log(fullAddress, typeof fullAddress);

console.log(`I live in the ${country} with ${trial}.`)

console.log( `Metro Manila is \n\n in the ${country}`);
console.log('John\'s identity');
console.log(`Metro

	Manila`);

let headCount = 23;
console.log (headCount);

let grade = 98.7;
console.log(grade, typeof grade);

let married = false;
console.log(typeof married);

let grades = [98.7,92.1,90.2,94.6];
console.log(grades, typeof grades);

console.log(grades[2], typeof grades[2]);

let details = ["john","smith",42, true];

let message = `${details[0]} and ${details[2]}`;
console.log(message);

const anime = ['one piece', 'one punch man','attack on titan'];
anime[0] = 'kimetsu no yaiba';
console.log(anime);


let objectGrades = {
	firstQuarter:98.7,
	secondQuarter: 92.1,
	thirdQuarter: 90.2,
	fourthQuarter: 94.6
};

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact:[90909,352543]
}

console.log(person.contact[0]);


// 1. Create 2 variables named firstName and lastName.
//                 -Add your first name and last name as strings to its appropriate variables.
//                 -Log your firstName and lastName variables in the console at the same time.

let firstName = 'Benjamin';
let lastName = 'Suarez';

console.log(firstName, lastName);

// 2. Create a variable called sentence.
//                 -Combine the variables to form a single string which would legibly and -understandably create a sentence that would say that you are a student of Zuitt.

//                 -Log the sentence variable in the console.


const sentence = `I am ${firstName} ${lastName} and I am a student of Zuitt.`;
console.log(sentence);

// 3. Create a variable with a group of data.
//                         -The group of data should contain names from your favorite food.

let favoriteFood = ['chocolate', 'noodles', 'fruits']
// 4. Create a variable which can contain multiple values of differing types and describes a single person.
//                         -This data type should be able to contain multiple key value pairs:
//                                 firstName: <value>
//                                 lastName: <value>
//                                 isDeveloper: <value>
//                                 hasPortfolio: <value>
//                                 age: <value>
let profile = {
		firstName: 'Benjamin',
		lastName: 'Suarez',
		isDeveloper: true,
		hasPortfolio: false,
		age: 31,
}


console.log(profile);

/*
let firstName = 'Benjamin';
let lastName = 'Suarez';


const sentence = `I am ${firstName} ${lastName}, and I am a student of Zuitt.`;
console.log(sentence);

let favoriteFood = ['chocolate', 'noodles', 'fruits'];

let profile = {
		firstName: 'Benjamin',
		lastName: 'Suarez',
		isDeveloper: true,
		hasPortfolio: false,
		age: 31,
}
*/
// NUMBERS
let numb1 = 5;
let numb2 = 6;
let numb3 = 5.5;
let numb4 = 0.5;
let numString1 = '5';
console.log(numb1 + numString1);


console.log(numb1%numb2);
console.log(numb2%numb1);

let girlfriend = null;
console.log(girlfriend);

function myFunction() {
	var nickName = "jane";
	//console.log(nickName);
}
myFunction();


function printName(name) {
	console.log(`My name is ${name}.`);
};
printName(print);

function displayFullName (fName,lName,age){
	console.log(`${fName} ${lName} is ${age}`);
};
displayFullName("Benjie","Suarez",31,45);


function createFullName (firstName,middleName,lastName){

	return `${firstName} ${middleName} ${lastName}`;
	// console.log(" I will no loger run bec. the function has been returned");
}

let fullName1 = createFullName("Tom", "Cruz", "Mapother");

console.log(fullName1);

let fullName2 = displayFullName("William","bradley","Pitt");

console.log(fullName2);

let fullName3 = createFullName("Jeffrey","Buid", "Helo");
console.log(fullName3);


// //MiniActivity

// function division(num1,num2) {
// 	return num1/num2;
// };
// let input1 = prompt("What is your first number?");
// let input2 = prompt("What is your second number?");

// let quotient = division(input1,input2);
// // console.log(`The result of the division is ${quotient}`); 
// alert(`The result of the division is ${quotient}`);


function argumentFunction(){
	console.log("printed");
}

function invokeFunction(){

}

invokeFunction(argumentFunction)














